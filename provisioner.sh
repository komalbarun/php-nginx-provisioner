# add repo
apt update -y #&& apt upgrade -y
apt install ca-certificates apt-transport-https software-properties-common -y
add-apt-repository ppa:ondrej/php -y
apt update -y

# add php
apt install php8.3-fpm php8.3 php8.3-mysql php8.3-mbstring php8.3-imap php8.3-xml php8.3-zip php8.3-curl -y

# add web server
apt install nginx -y

# add database server / optional
apt install mariadb-server -y

# other dependencies
apt install vnstat -y
ufw enable
ufw allow "Nginx Full"
ufw allow "openSSH"
ufw allow "mysql"

# Install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'edb40769019ccf227279e3bdd1f5b2e9950eb000c3233ee85148944e555d97be3ea4f40c3c2fe73b22f875385f6a5155') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer